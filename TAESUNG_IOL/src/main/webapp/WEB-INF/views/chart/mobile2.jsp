<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/views/include/lib.jsp"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="chrome=1">
<meta http-equiv="cache-control" content="max-age=0" />
<meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="expires" content="0" />
<meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
<meta http-equiv="pragma" content="no-cache" />
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
<link rel="shortcut icon" href="${ctxPath }/images/dulink_favicon.ico">

<title>Dash Board</title>
<script type="text/javascript">
	var ctxPath = "${ctxPath}";
	var targetWidth = 3840;
	var targetHeight = 2160;

	var originWidth = window.innerWidth;
	var originHeight = window.innerHeight;

	var contentWidth = originWidth;
	var contentHeight = targetHeight / (targetWidth / originWidth);

	var screen_ratio = getElSize(240);

	if (originHeight / screen_ratio < 9) {
		contentWidth = targetWidth / (targetHeight / originHeight)
		contentHeight = originHeight;
	};

	var marginWidth = (originWidth-contentWidth)/2;
	var marginHeight = (originHeight-contentHeight)/2;
	
	function getElSize(n) {
		return contentWidth / (targetWidth / n);
	};

	function setElSize(n) {
		return targetWidth / (contentWidth / n);
	};
</script>
<script type="text/javascript" src="${ctxPath }/js/jquery-1.11.1.min.js"></script>
<script type="text/javascript" src="${ctxPath }/js/jquery-ui.js"></script>
<script type="text/javascript" src="${ctxPath }/js/jquery.circle-diagram.js"></script>
<script src="${ctxPath }/js/jqMap.js"></script>
<script src="${ctxPath }/js/chart/highstock_data.js"></script>	
<script type="text/javascript" src="${ctxPath }/js/highstock.js"></script>
<script type="text/javascript" src="${ctxPath }/js/highcharts-3d.js"></script>
<script src="${ctxPath }/js/multicolor_series.js"></script>
<style type="text/css">
	body{
		background-color: black;
		margin: 0;
		padding: 0;
	}
	
	.mainTable{
		width: 100%;
		border-spacing: 10px;
	}
	.title_left{
		width: 20%;
		float: left;
	}
	
	.title_right{
		width: 20%;
		float: right;
	}
	
	.mainTable td{
		background-color: rgb(47,47,47);
		color: white;
		font-weight: bolder;
	}
	
	.tdHeader{
		color : white;
		background-color : rgb(34,34,34);
		padding : 5px;
	}
	.p{
		font-size: 50px;
		margin: 20 0 20 0;
	}
	.title{
		height: 40px;
	}
</style>
<script type="text/javascript">
	var dvcId = "${dvcId}";
	var demo_status = [];

	$(function(){
		setEl();
		getDvcData();
		statusBar("status");
	});
	
	function setEl(){
		$("#dvcName").css({
			//"position" : "absolute"
		});
	};
	
	function getDvcData(){
		var url = ctxPath + "/chart/getMachineData.do";
		var param = "dvcId=" + dvcId;
		
		$.ajax({
			url : url,
			data : param,
			type : "post",
			dataType : "json",
			success : function(data){
				$("#dvcName").html(data.name);
				$("#opRatio").html(data.opRatio);
				$("#cuttingRatio").html(data.cuttingRatio);
				$("#spdLoad").html(Number(data.spdLoad));
				$("#feedOverride").html(Number(data.feedOverride));
				
				setTimeout(getDvcData, 5000);
			}
		});
	};
	
	function statusBar(id){
		var perShapeGradient = {
				x1 : 0,
				y1 : 0,
				x2 : 1,
				y2 : 0
			};
			colors = Highcharts.getOptions().colors;
			colors = [ {
				linearGradient : perShapeGradient,
				stops : [ [ 0, 'rgb(100,238,92 )' ], [ 1, 'rgb(100,238,92 )' ] ]
			}, {
				linearGradient : perShapeGradient,
				stops : [ [ 0, 'rgb(250,210,80 )' ], [ 1, 'rgb(250,210,80 )' ] ]
			}, {
				linearGradient : perShapeGradient,
				stops : [ [ 0, 'rgb(231,71,79 )' ], [ 1, 'rgb(231,71,79 )' ] ]
			}, {
				linearGradient : perShapeGradient,
				stops : [ [ 0, '#8C9089' ], [ 1, '#8C9089' ] ]
			}, ]

			var height = window.innerHeight;

			var options = {
				chart : {
					type : 'coloredarea',
					backgroundColor : 'rgb(50,50,50)',
					height : 100,
					marginTop : -(height * 0.2),
				},
				credits : false,
				title : false,
				xAxis : {
					categories : [ 20, 0, 0, 0, 0, 0, 21, 0, 0, 0, 0, 0, 22, 0, 0, 0,
							0, 0, 23, 0, 0, 0, 0, 0, 24, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0,
							0, 2, 0, 0, 0, 0, 0, 3, 0, 0, 0, 0, 0, 4, 0, 0, 0, 0, 0, 5,
							0, 0, 0, 0, 0, 6, 0, 0, 0, 0, 0, 7, 0, 0, 0, 0, 0, 8, 0, 0,
							0, 0, 0, 9, 0, 0, 0, 0, 0, 10, 0, 0, 0, 0, 0, 11, 0, 0, 0,
							0, 0, 12, 0, 0, 0, 0, 0, 13, 0, 0, 0, 0, 0, 14, 0, 0, 0, 0,
							0, 15, 0, 0, 0, 0, 0, 16, 0, 0, 0, 0, 0, 17, 0, 0, 0, 0, 0,
							18, 0, 0, 0, 0, 0, 19, 0, 0, 0, 0, 0, 20, 0, 0, 0, 0, 0, ],
					labels : {

						formatter : function() {
							var val = this.value
							if (val == 0) {
								val = "";
							};
							return val;
						},
						style : {
							color : "white",
							fontSize : 8,
							fontWeight : "bold"
						},
					}
				},
				yAxis : {
					labels : {
						enabled : false,
					},
					title : {
						text : false
					},
				},
				tooltip : {
					enabled : false
				},
				plotOptions : {
					line : {
						marker : {
							enabled : false
						}
					}
				},
				legend : {
					enabled : false
				},
				series : []
			};

			$("#" + id).highcharts(options);

			var status = $("#" + id).highcharts();
			var options = status.options;
			options.series = [];
			options.title = null;
			options.exporting = false;

			
			getTimeData(id, options)
	}
	
	function getTimeData(id, options){
		var url = ctxPath + "/chart/getTimeData.do";
		var date = new Date();
		var year = date.getFullYear();
		var month = date.getMonth() + 1;
		var day = addZero(String(date.getDate()));
		
		var today = year + "-" + month + "-" + day;
		var param = "workDate=" + today + 
					"&dvcId=" + dvcId;
		
		$.ajax({
			url : url,
			dataType : "json",
			type : "post",
			data : param,
			success : function(data){
				var json = data.statusList;
				
				var color = "";
				
				var status = json[0].status;
				if(status=="IN-CYCLE"){
					color = "green"
				}else if(status=="WAIT"){
					color = "yellow";
				}else if(status=="ALARM"){
					color = "red";
				}else if(status=="NO-CONNECTION"){
					color = "gray";
				};
				
				options.series.push({
					data : [ {
						y : Number(20),
						segmentColor : color
					} ],
				});
				
				
				for(var i = 0; i <4; i++){
					options.series[0].data.push({
						y : Number(20),
						segmentColor : "gray"
					});
				}
				
				$(json).each(function(idx, data){
					if(data.status=="IN-CYCLE"){
						color = "green"
					}else if(data.status=="WAIT"){
						color = "yellow";
					}else if(data.status=="ALARM"){
						color = "red";
					}else if(data.status=="NO-CONNECTION"){
						color = "gray";
					};
					options.series[0].data.push({
						y : Number(20),
						segmentColor : color
					});
				});
				
				for(var i = 0; i < 144-json.length; i++){
					options.series[0].data.push({
						y : Number(20),
						segmentColor : "rgba(0,0,0,0)"
					});
				};
				
				status = new Highcharts.Chart(options);
			}
		});
	};
	
	var statusColor = [];
	
	function addZero(str){
		if(str.length==1) str = "0" + str;
		return str;
	};
</script>
</head>
<body oncontextmenu="setSlideMode(); return false">
	<!--Part2  -->
	<div id="part2" class="page">
		<table class="mainTable" >
			<tr>
				<Td align="center"  class="title" colspan="2">
					<img alt="" src="${ctxPath }/images/logo.png" class="title_left">
					<p style="font-size: 20px">개별 장비 상태</p>
				</Td>
			</tr>
			<tr>
				<td align="center" valign="top" colspan="2">
					<div class="tdHeader">장비</div>
					<label style="font-size: 30px; margin: 10px;" id="dvcName" ></label>	
					
				</td>
			</tr>
			<tr>
				<td colspan="2">
					<div class="tdHeader">상태</div>
					<div id="status"></div>
				</td>
			</tr>
			<Tr>
				<td align="center" valign="top" width="50%">
					<div class="tdHeader">Operation Ratio</div>
					<div class="p" id="opRatio"></div>
				</td>
				<td align="center" valign="top" width="50%">
					<div class="tdHeader">Cutting Time Ratio</div>
					<div class="p" id="cuttingRatio"></div>
				</td>						
			</Tr>
			<Tr>
				<td align="center" valign="top">
					<div class="tdHeader">Spindle Load</div>
					<div class="p" id="spdLoad"></div>
				</td>
				<td align="center" valign="top">
					<div class="tdHeader">Feed Override</div>
					<div class="p" id="feedOverride"></div>
				</td>						
			</Tr>
		</table>
	</div>
</body>
</html>