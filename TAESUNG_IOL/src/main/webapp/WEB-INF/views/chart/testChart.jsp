<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/views/include/lib.jsp"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="chrome=1">
<meta http-equiv="cache-control" content="max-age=0" />
<meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="expires" content="0" />
<meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
<meta http-equiv="pragma" content="no-cache" />
<link rel="shortcut icon" href="${ctxPath }/images/dulink_favicon.ico">

<title>Dash Board</title>
<script type="text/javascript">
	var ctxPath = "${ctxPath}";
	var targetWidth = 3840;
	var targetHeight = 2160;

	var originWidth = window.innerWidth;
	var originHeight = window.innerHeight;

	var contentWidth = originWidth;
	var contentHeight = targetHeight / (targetWidth / originWidth);

	var screen_ratio = getElSize(240);

	if (originHeight / screen_ratio < 9) {
		contentWidth = targetWidth / (targetHeight / originHeight)
		contentHeight = originHeight;
	};

	var marginWidth = (originWidth-contentWidth)/2;
	var marginHeight = (originHeight-contentHeight)/2;
	
	function getElSize(n) {
		return contentWidth / (targetWidth / n);
	};

	function setElSize(n) {
		return Math.floor(targetWidth / (contentWidth / n));
	};
</script>
<script type="text/javascript" src="${ctxPath }/js/jquery-1.11.1.min.js"></script>
<script type="text/javascript" src="${ctxPath }/js/jquery-ui.js"></script>
<script type="text/javascript" src="${ctxPath }/js/jquery.circle-diagram.js"></script>
<script src="${ctxPath }/js/jqMap.js"></script>
<script src="${ctxPath }/js/chart/highstock_data.js"></script>	
<script type="text/javascript" src="${ctxPath }/js/highstock.js"></script>
<script type="text/javascript" src="${ctxPath }/js/highcharts-3d.js"></script>
<script src="${ctxPath }/js/multicolor_series.js"></script>
<script src="${ctxPath }/js/custom_evt.js"></script> 
<script type="text/javascript">
$(function () {
	drawChart();
	drawChart2("barChart", array[0]);
	drawChart3("barChart2", array[0]);
	//getData();
	
	$("body").css({
		"overflow" : "auto"
	})
});

function drawLabel(){
	/* var startP, endP;
	for(var i = 0; i < $(".highcharts-series:nth(0) path").length; i++){
		var delId;
		delId = i;
		var color = $(".highcharts-series:nth(0) path:nth(" + i + ")").attr("stroke");
		if(color=="green"){
			startP = $(".highcharts-series:nth(0) path:nth(" + i + ")").offset().left
					
		}else if(color=="gray"){
			endP = $(".highcharts-series:nth(0) path:nth(" + i + ")").offset().left
			
			var circle = "<div id='div" + i + "'></div>";
			$("body").append(circle);
			$("#div" + i).css({
				"width" : 1,
				"height" : 50,
				"background-color" : "white",
				"position" : "absolute",
				"left" : (startP + endP)/2,
				"top" : 260
			});				
		}
	}; */
	
	var startP, endP;
	for(var i = 0; i <= ($(".highcharts-series:nth(0) path").length-1)/2; i++){
		var delId;
		delId = i;
		var color = $(".highcharts-series:nth(0) path:nth(" + i + ")").attr("stroke");
		if(color=="green"){
			startP = $(".highcharts-series:nth(0) path:nth(" + i + ")").offset().left

		}else if(color=="gray"){
			endP = $(".highcharts-series:nth(0) path:nth(" + i + ")").offset().left
			
		
			var circle = "<div id='div" + i + "' class='div'></div>";
			$("body").append(circle);
			$("#div" + i).css({
				"width" : endP - startP - 2,
				"height" : 50,
				"background-color" : "rgba(46,255,252,0.5)",
				"border-bottom" : "1px solid white",
				"border-left" : "1px solid white",
				"border-right" : "1px solid white",
				"position" : "absolute",
				"left" : startP,
				"top" : 300,
				"z-index" : -1
			});
		}
		
		if(i == ($(".highcharts-series:nth(0) path").length-1)/2-1 && color=="green"){
			endP = $(".highcharts-series:nth(0) path:nth(" + i + ")").offset().left
			
			var circle = "<div id='div" + i + "' class='div'></div>";
			$("body").append(circle);
			$("#div" + i).css({
				"width" : endP - startP - 2,
				"height" : 50,
				"background-color" : "rgba(46,255,252,0.5)",
				"border-bottom" : "1px solid white",
				"border-left" : "1px solid white",
				"border-right" : "1px solid white",
				"position" : "absolute",
				"left" : startP,
				"top" : 300,
				"z-index" : -1
			});
		}
	}; 
	
	//$("#div" + delId).remove();
};

function getData(){
	var url = ctxPath + "/chart/getData.do";
	
	$.ajax({
		url : url,
		type : "post",
		dataType : "json",
		success : function(data){
			
		}
	});
};

var hourLabel = [];
var minuteLabel = [];
function drawDetailChart(){
	$(".div").remove();
	
	for(var i = 0; i < 60; i++){
		minuteLabel.push(i);
		minuteLabel.push(0);
		minuteLabel.push(0);
		minuteLabel.push(0);
		minuteLabel.push(0);
		minuteLabel.push(0);
	};
	
	var perShapeGradient = {
			x1 : 0,
			y1 : 0,
			x2 : 1,
			y2 : 0
		};
		colors = Highcharts.getOptions().colors;
		colors = [ {
			linearGradient : perShapeGradient,
			stops : [ [ 0, 'rgb(100,238,92 )' ], [ 1, 'rgb(100,238,92 )' ] ]
		}, {
			linearGradient : perShapeGradient,
			stops : [ [ 0, 'rgb(250,210,80 )' ], [ 1, 'rgb(250,210,80 )' ] ]
		}, {
			linearGradient : perShapeGradient,
			stops : [ [ 0, 'rgb(231,71,79 )' ], [ 1, 'rgb(231,71,79 )' ] ]
		}, {
			linearGradient : perShapeGradient,
			stops : [ [ 0, '#8C9089' ], [ 1, '#8C9089' ] ]
		}, ]

		var height = window.innerHeight;

		var options = {
			chart : {
			/*	backgroundColor : 'rgba(0,0,0,0)',
				height : getElSize(800),
				width : getElSize(2500),
				marginTop : -(height * 0.2), */
				marginTop : 0,
				height : 300,
				width : window.innerWidth,
				backgroundColor : 'rgba(0,0,0,0)',
			},
			credits : false,
			title : false,
			xAxis : {
				categories : minuteLabel,
				labels : {
					formatter : function() {
						var val = this.value
						if (val == 0) {
							val = "";
						};
						return val;
					},
					style : {
						color : "white",
						fontSize : 10,
						fontWeight : "bold",
						cursor : "pointer"
					},
					events: {
                        click: function () {
                        	drawChart();
                        }
					}
				}
			},
			yAxis : {
				labels : {
					enabled : true,
					style : {
						color : "white",
						fontSize : 10,
						fontWeight : "bold",
						cursor : "pointer"
					},
				},
				title : {
					text : false
				},
			},
			tooltip : {
				enabled : false
			},
			plotOptions : {
				line : {
					marker : {
						enabled : false
					}
				},
//				series: {
//			        animation: false
//			    }
			},
			legend : {
				enabled : false
			},
			series : []
		};

	$("#chart").highcharts(options);
	
    var status = $("#chart").highcharts();
	var options = status.options;
	
	options.series = [];
	options.title = null;
	options.exporting = false;
	
	options.series.push({
		type : "coloredarea", 
		data : [ {
			y : Number(200),
			segmentColor : "green"
		} ],
	});
	
	options.series.push({
		type : "coloredline",
		lineWidth : 4,
		data : [ {
			y : Number(180),
			segmentColor : "black"
		} ],
	});
	
	var color = "";
	for(var i = 0; i < ((60*6)-1); i++){
		var n = Math.random()*10;
		if(n<8){
			color = "green";
		}else{
			color = "gray";
		};
		
		//bar
		options.series[0].data.push({
			y : Number(200),
			segmentColor : color
		});	
		
		//line
		options.series[1].data.push({
			y : Number(n*2) + 170,
			segmentColor : "black"
		});
	};
	
	
	status = new Highcharts.Chart(options);	
	
	setTimeout(drawLabel, 1000);
};

var startPoint = 20;
var n = 0;
var cellCnt = 24;
var c_options2;
var initColor = "green";

function drawChart(){
	$(".div").remove();
	for(var i = 0; i < 6*cellCnt; i++){
		hourLabel.push(startPoint);
		hourLabel.push(0);
		hourLabel.push(0);
		hourLabel.push(0);
		hourLabel.push(0);
		hourLabel.push(0);
		
		startPoint++;
		if(startPoint==25) startPoint = 1;
	};
	
	var perShapeGradient = {
			x1 : 0,
			y1 : 0,
			x2 : 1,
			y2 : 0
		};
		colors = Highcharts.getOptions().colors;
		colors = [ {
			linearGradient : perShapeGradient,
			stops : [ [ 0, 'rgb(100,238,92 )' ], [ 1, 'rgb(100,238,92 )' ] ]
		}, {
			linearGradient : perShapeGradient,
			stops : [ [ 0, 'rgb(250,210,80 )' ], [ 1, 'rgb(250,210,80 )' ] ]
		}, {
			linearGradient : perShapeGradient,
			stops : [ [ 0, 'rgb(231,71,79 )' ], [ 1, 'rgb(231,71,79 )' ] ]
		}, {
			linearGradient : perShapeGradient,
			stops : [ [ 0, '#8C9089' ], [ 1, '#8C9089' ] ]
		}, ]

		var height = window.innerHeight;

		var options = {
			chart : {
			//	type : 'coloredarea',
			/*	backgroundColor : 'rgba(0,0,0,0)',
				height : getElSize(800),
				width : getElSize(2500),
				marginTop : -(height * 0.2), */
				//marginTop : 10,
				height : 300,
				width : window.innerWidth,
				backgroundColor : 'rgba(0,0,0,0)'
			},
			credits : false,
			title : false,
			xAxis : {
				categories : hourLabel,
				labels : {
					formatter : function() {
						var val = this.value
						if (val == 0) {
							val = "";
						};
						return val;
					},
					style : {
						color : "white",
						fontSize : 10,
						fontWeight : "bold",
						cursor : "pointer"
					},
					events: {
                        click: function () {
                            drawDetailChart();
                        }
					}
				}
			},
			yAxis : {
				labels : {
					enabled : true,
					style : {
						color : "white",
						fontSize : 10,
						fontWeight : "bold",
						cursor : "pointer"
					},
				},
				title : {
					text : false
				},
			},
			tooltip : {
				enabled : false
			},
			plotOptions : {
				line : {
					marker : {
						enabled : false
					}
				},
//				series: {
//			        animation: false
//			    }
			},
			legend : {
				enabled : false
			},
			series : []
		};

	$("#chart").highcharts(options);
	
    var status = $("#chart").highcharts();
	var options = status.options;
	
	options.series = [];
	options.title = null;
	options.exporting = false;
	
	options.series.push({
		type : "coloredarea", 
		data : [ {
			y : Number(200),
			segmentColor : initColor
		} ],
	});
	
	options.series.push({
		type : "coloredline", 
		lineWidth : 4,
		data : [ {
			y : Number(180),
			segmentColor : "black"
		} ],
	});
	
	for(var i = 0; i < (6*cellCnt) -1 ; i++){
		var n = Math.random()*10;
		if(n<8){
			color = "green";
		}else{
			color = "gray";
		};
		
		//bar
		options.series[0].data.push({
			y : Number(200),
			segmentColor : color
		});	
		
		//line
		options.series[1].data.push({
			y : Number(n*2) + 170,
			segmentColor : "black"
		});
	};
	
	
	status = new Highcharts.Chart(options);
	
	setTimeout(drawLabel, 1000);
};

var cellPos = new Array();
var labelPos = new Array();

var startTimeLabel = new Array();
var startHour = 20;
var startMinute = 3;
function drawChart2(id, dvcId){
	var m0 = "",
	m02 = "",
	m04 = "",
	m06 = "",
	m08 = "",
	
	m1 = "";
	m12 = "",
	m14 = "",
	m16 = "",
	m18 = "",
	
	m2 = "";
	m22 = "",
	m24 = "",
	m26 = "",
	m28 = "",
	
	m3 = "";
	m32 = "",
	m34 = "",
	m36 = "",
	m38 = "",
	
	m4 = "";
	m42 = "",
	m44 = "",
	m46 = "",
	m48 = "",
	
	m5 = "";
	m52 = "",
	m54 = "",
	m56 = "",
	m58 = "";
	var n = Number(startHour);
if(startMinute!=0) n+=1;

for(var i = 0, j = n ; i < 24; i++, j++){
	eval("m" + startMinute + "=" + j);
	
	startTimeLabel.push(m0);
	startTimeLabel.push(m02);
	startTimeLabel.push(m04);
	startTimeLabel.push(m06);
	startTimeLabel.push(m08);
	
	startTimeLabel.push(m1);
	startTimeLabel.push(m12);
	startTimeLabel.push(m14);
	startTimeLabel.push(m16);
	startTimeLabel.push(m18);
	
	startTimeLabel.push(m2);
	startTimeLabel.push(m22);
	startTimeLabel.push(m24);
	startTimeLabel.push(m26);
	startTimeLabel.push(m28);
	
	startTimeLabel.push(m3);
	startTimeLabel.push(m32);
	startTimeLabel.push(m34);
	startTimeLabel.push(m36);
	startTimeLabel.push(m38);
	
	startTimeLabel.push(m4);
	startTimeLabel.push(m42);
	startTimeLabel.push(m44);
	startTimeLabel.push(m46);
	startTimeLabel.push(m48);
	
	startTimeLabel.push(m5);
	startTimeLabel.push(m52);
	startTimeLabel.push(m54);
	startTimeLabel.push(m56);
	startTimeLabel.push(m58);
	
	if(j==24){ j = 0}
};

	var perShapeGradient = {
		x1 : 0,
		y1 : 0,
		x2 : 1,
		y2 : 0
	};
	colors = Highcharts.getOptions().colors;
	colors = [ {
		linearGradient : perShapeGradient,
		stops : [ [ 0, 'rgb(100,238,92 )' ], [ 1, 'rgb(100,238,92 )' ] ]
	}, {
		linearGradient : perShapeGradient,
		stops : [ [ 0, 'rgb(250,210,80 )' ], [ 1, 'rgb(250,210,80 )' ] ]
	}, {
		linearGradient : perShapeGradient,
		stops : [ [ 0, 'rgb(231,71,79 )' ], [ 1, 'rgb(231,71,79 )' ] ]
	}, {
		linearGradient : perShapeGradient,
		stops : [ [ 0, '#8C9089' ], [ 1, '#8C9089' ] ]
	}, ]
	
	var height = window.innerHeight;
	
	var options = {
		chart : {
			type : 'coloredarea',
			backgroundColor : 'rgba(0,0,0,0)',
			height : getElSize(800),
			width : getElSize(2500),
			marginTop : -(height * 0.2),
		},
		credits : false,
		title : false,
		xAxis : {
			categories : startTimeLabel,
			labels : {
				step: 1,
				formatter : function() {
					var val = this.value
	
					return val;
				},
				style : {
					color : "white",
					fontSize : getElSize(60),
					fontWeight : "bold"
				},
			}
		},
		yAxis : {
			labels : {
				enabled : false,
			},
			title : {
				text : false
			},
		},
		tooltip : {
			enabled : false
		},
		plotOptions : {
			line : {
				marker : {
					enabled : false
				}
			}
		},
		legend : {
			enabled : false
		},
		series : []
	};
	
	$("#" + id).highcharts(options);
	
	var status = $("#" + id ).highcharts();
	c_options = status.options;
	
	c_options.series = [];
	c_options.title = null;
	c_options.exporting = false;
	

	
	getTimeData(dvcId);
	
};

function drawChart3(id, dvcId){
	var m0 = "",
	m02 = "",
	m04 = "",
	m06 = "",
	m08 = "",
	
	m1 = "";
	m12 = "",
	m14 = "",
	m16 = "",
	m18 = "",
	
	m2 = "";
	m22 = "",
	m24 = "",
	m26 = "",
	m28 = "",
	
	m3 = "";
	m32 = "",
	m34 = "",
	m36 = "",
	m38 = "",
	
	m4 = "";
	m42 = "",
	m44 = "",
	m46 = "",
	m48 = "",
	
	m5 = "";
	m52 = "",
	m54 = "",
	m56 = "",
	m58 = "";
	var n = Number(startHour);
if(startMinute!=0) n+=1;

for(var i = 0, j = n ; i < 24; i++, j++){
	eval("m" + startMinute + "=" + j);
	
	startTimeLabel.push(m0);
	startTimeLabel.push(m02);
	startTimeLabel.push(m04);
	startTimeLabel.push(m06);
	startTimeLabel.push(m08);
	
	startTimeLabel.push(m1);
	startTimeLabel.push(m12);
	startTimeLabel.push(m14);
	startTimeLabel.push(m16);
	startTimeLabel.push(m18);
	
	startTimeLabel.push(m2);
	startTimeLabel.push(m22);
	startTimeLabel.push(m24);
	startTimeLabel.push(m26);
	startTimeLabel.push(m28);
	
	startTimeLabel.push(m3);
	startTimeLabel.push(m32);
	startTimeLabel.push(m34);
	startTimeLabel.push(m36);
	startTimeLabel.push(m38);
	
	startTimeLabel.push(m4);
	startTimeLabel.push(m42);
	startTimeLabel.push(m44);
	startTimeLabel.push(m46);
	startTimeLabel.push(m48);
	
	startTimeLabel.push(m5);
	startTimeLabel.push(m52);
	startTimeLabel.push(m54);
	startTimeLabel.push(m56);
	startTimeLabel.push(m58);
	
	if(j==24){ j = 0}
};

	var perShapeGradient = {
		x1 : 0,
		y1 : 0,
		x2 : 1,
		y2 : 0
	};
	colors = Highcharts.getOptions().colors;
	colors = [ {
		linearGradient : perShapeGradient,
		stops : [ [ 0, 'rgb(100,238,92 )' ], [ 1, 'rgb(100,238,92 )' ] ]
	}, {
		linearGradient : perShapeGradient,
		stops : [ [ 0, 'rgb(250,210,80 )' ], [ 1, 'rgb(250,210,80 )' ] ]
	}, {
		linearGradient : perShapeGradient,
		stops : [ [ 0, 'rgb(231,71,79 )' ], [ 1, 'rgb(231,71,79 )' ] ]
	}, {
		linearGradient : perShapeGradient,
		stops : [ [ 0, '#8C9089' ], [ 1, '#8C9089' ] ]
	}, ]
	
	var height = window.innerHeight;
	
	var options = {
		chart : {
			type : 'coloredarea',
			backgroundColor : 'rgba(0,0,0,0)',
			height : getElSize(800),
			width : getElSize(2500),
		//	marginTop : -(height * 0.2),
		},
		credits : false,
		title : false,
		xAxis : {
			categories : startTimeLabel,
			labels : {
				step: 1,
				formatter : function() {
					var val = this.value
	
					return val;
				},
				style : {
					color : "white",
					fontSize : getElSize(60),
					fontWeight : "bold"
				},
			}
		},
		yAxis : {
			gridLineWidth: 0,
			lineWidth: 0,
			minorGridLineWidth: 0,
			lineColor: 'transparent',
			min : 0,
			max : 20,
			labels : {
				enabled : false,
			},
			title : {
				text : false
			},
		},
		tooltip : {
			enabled : false
		},
		plotOptions : {
			series : {
				marker : {
					enabled : false
				}
			}
		},
		legend : {
			enabled : false
		},
		series : []
	};
	
	$("#" + id).highcharts(options);
	
	var status = $("#barChart2").highcharts();
	c_options2 = status.options;
	
	c_options2.series = [];
	c_options2.title = null;
	c_options2.exporting = false;
	

	
	getTimeData2(dvcId);
	
};

var array = [2,27,8,1,25,30];
//var array = [2];
var c_options;
var z = 0;
var bar_height = 0;
var barHEIGHT = 20;
var opacity = 1/array.length;
function getTimeData(dvcId){
	var url = ctxPath + "/chart/getTimeData.do";
	var date = new Date();
	var year = date.getFullYear();
	var month = date.getMonth() + 1;
	var day = addZero(String(date.getDate()));
	var hour = date.getHours();
	var minute = addZero(String(date.getMinutes())).substr(0,1);
	
	
	if(hour>startHour || (hour>=startHour &&minute >= startMinute)){
		day = addZero(String(new Date().getDate()+1));
	};
	
	
	var today = year + "-" + month + "-" + day;
	var param = "workDate=" + today + 
				"&dvcId=" + dvcId;
	$.ajax({
		url : url,
		dataType : "json",
		type : "post",
		data : param,
		success : function(data){
			var json = data.statusList;
			var color = "";
			
			var status = json[0].status;
			if(status=="IN-CYCLE"){
				color = "rgba(255,0,0," + opacity + ")";
			}else if(status=="WAIT"){
				color = "rgba(255,255,0," + opacity+ ")";
			}else if(status=="ALARM"){
				color = "rgba(0,255,0," + opacity +")";
			}else if(status=="NO-CONNECTION"){
				color = "gray";
			};
			
			c_options.series.push({
				data : [ {
					y : Number(20),
					segmentColor : color
				} ],
			});
			
			$(json).each(function(idx, data){
				if(data.status=="IN-CYCLE"){
					color = "rgba(0,255,0," + opacity+ ")";
				}else if(data.status=="WAIT"){
					color = "rgba(255,255,0," + opacity + ")";
				}else if(data.status=="ALARM"){
					color = "rgba(255,0,0," + opacity+ ")";
				}else if(data.status=="NO-CONNECTION"){
					color = "gray";
				};
				c_options.series[z].data.push({
					y : Number(20),
					segmentColor : color
				});
			});
			
			for(var i = 0; i < 719-json.length; i++){
				c_options.series[z].data.push({
					y : Number(20),
					segmentColor : "rgba(0,0,0,0)"
				});
			};
			
			z++;
			status = new Highcharts.Chart(c_options);
			
			//drawLabelPoint();
			
			if(z <= array.length-1){
				getTimeData(array[z])
			}
		},error : function(e1,e2,e3){
		}
	});
};

function addZero(str){
	if(str.length==1) str = "0" + str;
	return str;
};

var z2 = 0;
var bar_height = 20/array.length;
function getTimeData2(dvcId){
	var url = ctxPath + "/chart/getTimeData.do";
	var date = new Date();
	var year = date.getFullYear();
	var month = date.getMonth() + 1;
	var day = addZero(String(date.getDate()));
	var hour = date.getHours();
	var minute = addZero(String(date.getMinutes())).substr(0,1);
	
	
	if(hour>startHour || (hour>=startHour &&minute >= startMinute)){
		day = addZero(String(new Date().getDate()+1));
	};
	
	
	var today = year + "-" + month + "-" + day;
	var param = "workDate=" + today + 
				"&dvcId=" + dvcId;
	$.ajax({
		url : url,
		dataType : "json",
		type : "post",
		data : param,
		success : function(data){
			var json = data.statusList;
			var color = "";
			
			var status = json[0].status;
			if(status=="IN-CYCLE"){
				color = "rgba(255,0,0,1)";
			}else if(status=="WAIT"){
				color = "rgba(255,255,0,1)";
			}else if(status=="ALARM"){
				color = "rgba(0,255,0,1)";
			}else if(status=="NO-CONNECTION"){
				color = "gray";
			};
			
			c_options2.series.push({
				data : [ {
					y : Number(barHEIGHT),
					segmentColor : color
				} ],
			});
			
			$(json).each(function(idx, data){
				if(data.status=="IN-CYCLE"){
					color = "rgba(0,255,0,1)";
				}else if(data.status=="WAIT"){
					color = "rgba(255,255,0,1)";
				}else if(data.status=="ALARM"){
					color = "rgba(255,0,0,1)";
				}else if(data.status=="NO-CONNECTION"){
					color = "gray";
				};
				c_options2.series[z2].data.push({
					y : Number(barHEIGHT),
					segmentColor : color
				});
			});
			
			for(var i = 0; i < 719-json.length; i++){
				c_options2.series[z2].data.push({
					y : Number(barHEIGHT),
					segmentColor : "rgba(0,0,0,0)"
				});
			};
			
			status = new Highcharts.Chart(c_options2);
			//drawLabelPoint();
			
			z2++;
			
			if(z2 <= array.length-1){
				getTimeData2(array[z2])
			};
			
			barHEIGHT-=bar_height;
		},error : function(e1,e2,e3){
		}
	});
};

</script>
<link rel="stylesheet" href="${ctxPath }/css/card.css">
</head>

<body >
	<br><br>
	<div id="chart"></div>
	<br><br>
	<div id="barChart"></div>
	<br><br>
	<div id="barChart2"></div>
</body>
</html>