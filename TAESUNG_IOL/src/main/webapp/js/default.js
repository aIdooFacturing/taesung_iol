//machineList id, company, name, comIp, x, y

var panel = false;
var colors, colors_a;
var subMenuTr = false;
var status3, status4;
var cMenu;
var circleWidth = contentWidth * 0.07;
$(function() {
	setEl();
	// drawStockChart();
	getMachineList();
	
	drawBarChart("container");
	pieChart();
	statusChart3("status3_1");
	statusChart4("status3_2");
	
	evtBind();

	$("#panel_table tr:nth(1) td:nth(0) img").css({
		"border" : getElSize(20) + "px solid rgb(33,128,250)"
	});
	
	cMenu = $("#panel_table tr:nth(1) td:nth(0) img").attr("id");
	//$("#panel_table td:nth(" + (cPage - 1) + ")").removeClass("unSelected_menu");
	//$("#panel_table td:nth(" + (cPage - 1) + ")").addClass("activeMenu");
	
	setTimeout(function(){
		//statusBarAnim("status3_1", 43);
		//statusBarAnim("status3_2", 45);
	}, 1000);
	
//	addSeries();
});

var block = 1/6;
function addSeries(){
	var date = new Date();
	var hour = date.getHours();
	var minute = String(date.getMinutes()).substr(0,1);
	var bar;
	
	if(minute.length==1){
		minute = 0;
	}else{
		minute = block * minute;
	};

	status3.addSeries({
		data : [ 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 ],
		color : colors[0]
	});
};

function select_remove_card(){
	$(".removeIcon").toggle(500);
};

function statusBarAnim(chart, idx){
	var startPoint = $($(".highcharts-grid")[idx]).offset().left;
	var endPoint = $("#" + chart).offset().left + $("#" + chart).width()-10;
	var width = endPoint - startPoint;
	var linePos = width/24;
	
	var date = new Date();
	var hour = date.getHours();
	var minute = String(date.getMinutes());
	
	if(minute.length==1){
		minute = 0;
	}else{
		minute = minute.substr(0,1);
		minute = (linePos/6) * minute;
	};

	if(hour<20){
		linePos = (hour+4) * linePos + startPoint + minute;
	}else{
		linePos = (hour-20) * linePos + startPoint + minute;
	};
	
	$("#verticalLine" + idx).css({
		"left" : linePos,
		"height" : $("#status3_1").height()-45,
		"z-index" : 999999999,
		"display" : "inline"
	});
	
	$("#verticalLine" + idx).css("box-shadow", "0px 0px " + lineWidth + "px " + lineWidth + "px rgb(86,215,250)");

	if (toggle) {
		lineWidth -= getElSize(5)/5;
		if (lineWidth <= 0) {
			toggle = false;
		};
	} else if (!toggle) {
		lineWidth += getElSize(5)/5;
		if (lineWidth >= getElSize(5)) {
			toggle = true;
		};
	};
		
	setTimeout(function(){
		statusBarAnim(chart, idx);
	}, 80)
};

var lineWidth = getElSize(5);
var toggle = true;

var preFolderId;
var folderArray = new Array();
function getFolderId(folderId){
	var result = false;
	for(var i = 0; i < folderArray.length; i++){
		if(folderArray[i]==folderId){
			result = true;
		};
	};
	return result;
};

function getFolderList(folderId){
	folderArray.push(folderId);
	var url = ctxPath + "/chart/getFolderList.do";
	var param = "folderId=" + folderId;
	$.ajax({
		url : url,
		data : param,
		dataType : "json",
		type : "post",
		success : function(data){
			var json = data.folderList;
			
			var left = getElSize(json[0].x) + marginWidth;
			var top = getElSize(json[0].y) + marginHeight;
			
			var box = document.createElement("div");
			box.setAttribute("class", "box");
			
			var folder = document.createElement("div");
			folder.setAttribute("class", "folder");
			folder.setAttribute("id", "folder" + folderId);
			
			var folder_name = json[0].name;
			$(json).each(function (idx, data){
				if(idx<9){
					var img = document.createElement("img");
					img.setAttribute("src", ctxPath + "/images/shopLayOut/" + data.pic + ".png");
					img.setAttribute("class", "thumb");
					
					folder.appendChild(img);
				};
			});
			
			setFolderAttr(box, folder, left, top);
			addFolderName(folder_name, box);
		}
	});
};

function addFolderName(folder_name, box){
	var idx = $(box).children().attr("id").substr(6);
	
	var nameDiv = document.createElement("div");
	var nameNode = document.createTextNode(folder_name);
	nameDiv.appendChild(nameNode);
	nameDiv.setAttribute("id", "folder_name_" + idx);
	
	nameDiv.style.cssText = "font-size:" + getElSize(50) + "; " + 
							"color : white; " + 
							"margin-top : " + getElSize(30);
							
	$(box).append(nameDiv);
	$(nameDiv).click(modifyFolderName);
};

var modifyName = "";
function modifyFolderName(){
	modifyName = this.id;
	var name = $(this).html();
	showCorver();
	$("#folder_name").val(name);
	$("#nameBox").toggle(function(){
		$("#folder_name").focus();
	});
	
};

function setFolderAttr(box, folder, left, top){
	box.appendChild(folder);
	$("#cards").prepend(box);
	
	$(".thumb").css({
		"margin" : getElSize(5),
		"width" : "28%",
		"height" :"28%",
		"display" : "inline",
		"-webkit-border-radius": "100%"
	});
	
	$(box).css({
		"padding" : 0,
		"left" : left,
		"top" : top,
		"position" : "absolute",
		"margin" : getElSize(20),
		"width" : circleWidth,
		"height" : circleWidth,
		"cursor" : "pointer"
	});
	
	$(folder).css({
		"margin" : 0,
		"width" : circleWidth - getElSize(20),
		"height" : circleWidth - getElSize(20),
		"background-color" : "#575757",
		"border" : getElSize(10) + "px solid white",
		"border-radius" : getElSize(30)
	});
	
	$(folder).click(openFolder);
	
	$(folder).droppable({
		accept : ".machine_icon",
		drop: function( event, ui ) {
			addDvc2Folder(this);
			this.id;
			$("#" + selected_circle).animate({
				"width" : 0,
				"height" : 0
			},function(){
				$("#" + selected_circle).remove();
			});
			$(this).animate({
				"width" : circleWidth,
				"height" : circleWidth,
			});	
		},
		over: function( event, ui ) {
			$(this).animate({
				"width" : circleWidth * 1.2,
				"height" : circleWidth * 1.2,
			});
		},
		out: function( event, ui ) {
			$(this).animate({
				"width" : circleWidth,
				"height" : circleWidth
			});
		}
	});
};

var alarmList = new Array();
var machineList = new Array();

function getMachineList(){
	var url = ctxPath + "/chart/getMachineList.do";
	
	$.ajax({
		url : url,
		dataType : "json",
		type : "post",
		success :function(data){
			var json = data.machineList;
			
			var circle = "";
			var border_color = "";
			
			$(json).each(function (idx, data){
				var machine = new Array();
				machine.push("wrap" + data.id);
				machine.push(data.company)
				machine.push(data.name);
				machine.push(data.camIp);
				machine.push(data.x);
				machine.push(data.y);

				//machineList id, company, name, comIp, x, y
				machineList.push(machine);
				
				if(data.status.toLowerCase()=="in-cycle"){
					border_color = 'green';
				}else if(data.status.toLowerCase()=="wait"){
					border_color = 'yellow';
				}else if(data.status.toLowerCase()=="alarm"){
					border_color = 'red';
				}else if(data.status.toLowerCase()=="no-connection"){
					border_color = 'gray';
				};
				
				if(data.folderId==null){
					circle += "<div class='machine_icon' id='circle" + data.id + "' style='left : " + (getElSize(data.x) + marginWidth) + "; top:" + (getElSize(data.y) + marginHeight) +"';>" +  
								"<div class='circle' style='box-shadow : 0px 0px " + getElSize(10) + "px " + getElSize(10) + "px " + border_color + "'>" + 
									"<img src=" + ctxPath + "/images/shopLayOut/" + data.pic + ".png class='icon' id=img" + data.id + ">" +
								"</div>" + 
								"<div class='machine_name' id='machine_name_" + data.id + "' style='color:white; font-size:" + getElSize(50) + "; margin-top : " + getElSize(30) + "'>" + data.name + "</div>" + 
							"</div>";

					if(border_color=="red") alarmList.push("circle" + data.id);
				}else{
					if(!getFolderId(data.folderId)) getFolderList(data.folderId);
				};
			});
			
			$("#cards").html(circle);
			$(".machine_name").click(modifyFolderName);
			
			setEl();
			
			borderAnim();
				
//			"width" : contentWidth * 0.07,
//			"height" : contentHeight * 0.15
			
			$(".icon").css({
				"width": circleWidth * 0.6,
				"height": circleWidth * 0.6,
				"margin-top" : getElSize(50),
			});
			
			$(".icon").each(function(idx, data){
				$(data).css({
					"left" : (circleWidth/2) - $(data).width()/2,
					"top" : (circleWidth/2) - $(data).height()/2,
				});
			});
			
			$(".machine_cam").click(viewCam);
			
			$(".machine_icon").draggable({
				start: function(){
					$(this).css("z-index",2);
					selected_circle = this.id;
				},
				stop : function(){
					var offset = $(this).offset();
		            var x = offset.left;
		            var y = offset.top;
		            var id = this.id;
		            id = id.substr(6);
		            
		            $(this).css("z-index",1);
		           //setCardPos(id, x, y);
				},
			},{ 
				revert: "invalid" 
			});
			
			$(".machine_icon").droppable({
				accept : ".machine_icon",
				drop: function( event, ui ) {
					dropped_circle = this.id;
					makeNewFolder();
					$("#" + dropped_circle).hide();
					$("#" + selected_circle).hide();
				},
				over: function( event, ui ) {
					$("#" + this.id + " .circle").animate({
						"width" : circleWidth * 1.2,
						"height" : circleWidth * 1.2,
					});
				},
				out: function( event, ui ) {
					$("#" + this.id + " .circle").animate({
						"width" : circleWidth,
						"height" : circleWidth
					});
				}
			});
		}
	});
};

function chkIconCnt(obj){
	var result;
	if($(obj).context.childElementCount<9){
		result = true;
	}else{
		result = false;
	}
	return result;
};


function showCorver(){
	$("#corver").css({
		"z-index":4,
		"opacity":0.7
	});
};

function hideCorver(){
	$("#corver").css({
		"z-index":-1,
	});
};

function modifyElName(){
	var url;
	var id =  modifyName.substr(modifyName.lastIndexOf("_")+1);
	var name = $("#folder_name").val();
	var param = "id=" + id + 
				"&name=" + name;
	
	var ty = modifyName.substr(0,modifyName.lastIndexOf("_name"));
	if(ty=="folder"){
		url = ctxPath + "/chart/modifyFolderName.do";
	}else {
		url = ctxPath + "/chart/modifyMachineName.do";
	};
	
	$.ajax({
		url : url,
		data : param,
		type : "post",
		success : function(){
			hideCorver();
			$("#folder_name").val("");
			$("#nameBox").toggle(500);
			$("#" + modifyName).html(name);
			modifyName = "";
		}
	});
	
};

function addFolder2DB(){
	var url = ctxPath + "/chart/makeFolder.do";
	var name = $("#folder_name").val();
	if(modifyName!=""){
		modifyElName();
		return;
	};
	
	var dvcList = [selected_circle.substr(6), dropped_circle.substr(6)];
	var param = "name=" + name + 
				"&dvcId=" + dvcList + 
				"&x=" + Math.floor(setElSize(folder_left-marginWidth)) +
				"&y=" + Math.floor(setElSize(folder_top-marginHeight));
	
	$.ajax({
		url : url,
		data : param,
		dataType : "text",
		type : "post",
		success : function(data){
			selected_folder.setAttribute("id", "folder" + data);
			hideCorver();
			$("#folder_name").val("");
			$("#nameBox").toggle(500);
			
			$("#" + selected_circle).remove();
			
			//이름 표시
			addFolderName(name, selectedFolder);
		}
	});
	
};

var selectedFolder;
var folder_left;
var folder_top;
var folder_width;
var folder_height;

function makeNewFolder(){
	showCorver();
	$("#nameBox").toggle(function(){
		$("#folder_name").focus();
	});
	
	folder_left = $("#" + dropped_circle).offset().left + getElSize(30);
	folder_top = $("#" + dropped_circle).offset().top - getElSize(20);
	
	var folder = document.createElement("div");
	folder.setAttribute("class", "folder");
	
	var box = document.createElement("div");
	box.setAttribute("class", "box");
	
	var img1 = document.createElement("img");
	img1.setAttribute("src", $("#" + selected_circle + " img").attr("src"));
	img1.setAttribute("class", "thumb");
	var img2 = document.createElement("img");
	img2.setAttribute("src", $("#" + dropped_circle + " img").attr("src"));
	img2.setAttribute("class", "thumb");
	
	folder.appendChild(img1);
	folder.appendChild(img2);
	
	selectedFolder = box;

	selected_folder = folder;
	
	setFolderAttr(box, folder, folder_left, folder_top);
};

function removeCard(){
	var del_confirm = confirm("Are you sure you want to delete?");
	if(!del_confirm) return;
	
	var url  = ctxPath + "/chart/removeCard.do";
	var id = this.id.substr(1);
	var param = "id=" + id;
	$("#wrap" + id).toggle(500);
	
	$.ajax({
		url : url,
		data : param,
		type : "post",
		success : function(){
		}
	});
};

function getCardList(folderId){
	var url = ctxPath + "/chart/getCardsList.do";
	var param = "folderId=" + folderId;
	$.ajax({
		url : url,
		dataType : "json",
		type : "post",
		data : param,
		success :function(data){
			var json = data.machineList;
			
			var card = "";
			var border_color = "";
			
			$(json).each(function (idx, data){
				var machine = new Array();
				machine.push("wrap" + data.id);
				machine.push(data.company)
				machine.push(data.name);
				machine.push(data.camIp);
				machine.push(data.x);
				machine.push(data.y);
				
				//machineList id, company, name, comIp, x, y
				machineList.push(machine);
				
				if(data.status.toLowerCase()=="in-cycle"){
					border_color = 'green';
				}else if(data.status.toLowerCase()=="wait"){
					border_color = 'yellow';
				}else if(data.status.toLowerCase()=="alarm"){
					border_color = 'red';
				}else if(data.status.toLowerCase()=="no-connection"){
					border_color = 'gray';
				};
				
				var cam;
				if(idx==3||idx==7||idx==9){
					cam = "<video src='http://106.240.234.114:8080/video/video_3_3.mp4' class='machine_cam video' autoplay loop='loop' muted='muted' id=com" + data.id + ">";
				}else{
					cam = "<img src='http://" + data.camIp + "/mjpg/1/video.mjpg' class='machine_cam' id=cam" + data.id + " >";
				};
				
				card += "<div class='wrap' id=wrap" + data.id + " style='box-shadow : 0px 0px " + getElSize(10) + "px " + getElSize(10) + "px " + border_color + "'>" +
							"<img src="  + ctxPath + "/images/remove3.png class='removeIcon' id=r" + data.id + " style='position:absolute; z-index:10; left : " + getElSize(10) + "'>" +
							"<div class='back'>" + 
								"<img src=" + ctxPath + "/images/shopLayOut/card_back.png class='card_back'>" + 
							"</div>" + 
							"<div class='front'>" +  
								"<table  class='card_table'>" +  
									"<tr >" + 
										"<td   colspan='3'>" + 
											"<img src=" + ctxPath + "/images/company/" + data.company.toLowerCase() + ".png class='logo'> " + "<span style='float:right; font-weight:bolder; font-size:" + getElSize(40) + "'>" + data.name + "</span>" + 
										"</td>" +
									"</tr>" + 
									"<tr style='height : " + contentHeight * 0.15 + "';>" +
										"<td valign='middle' style='width:" +  getElSize(200) + "'>" + 
											"<img src=" + ctxPath + "/images/shopLayOut/" + data.pic + ".png class='card_icon' id=img" + data.id + ">" + 
										"</td>" + 
										"<td align='center'>" + 
											cam +  
										"</td>" +
										"<td style='width:" +  getElSize(100) + "'><div class='opRatio'>85</div></td>" + 
									"</tr>" +
									"<tr>" + 
										"<td colspan='3'>" +
											"<div style='position:absolute; right:0; z-index:10; font-size:" + getElSize(20) + "' class='opTime'>14:10</div>" + 
											"<div id=card_status" + idx + " style='width: 100%' class='card_status'></div>" +
											"<div id=card_status" + idx + "_total style='width: 100%' class='card_status card_status_total'></div>" +
										"</td>" + 
									"</tr>" +
								"</table>" + 
							"</div>" + 
						"</div>";
				
			//	if(border_color=="red") alarmList.push("wrap" + data.id);
			});
			
			$("#board").html(card);
			
			setEl();
			//borderAnim();
				
//			"width" : contentWidth * 0.07,
//			"height" : contentHeight * 0.15
			$(".removeIcon").css({
				"width" : getElSize(100),
				"display" : "none"
			});
			
			$(".removeIcon").click(removeCard);
			
			$(".card_icon").css("width" , contentWidth * 0.07 * 0.8);
			$(".card_icon").each(function(idx, data){
				statusBar("card_status" + idx);
				statusBarTotal("card_status" + idx + "_total");
			});
			
			$(".machine_cam").click(viewCam);
			$(".card_icon").click(flipCard);
//			$(".wrap").draggable({
//				stop : function(){
//					var offset = $(this).offset();
//		            var x = offset.left;
//		            var y = offset.top;
//		            var id = this.id;
//		            id = id.substr(4);
//		            
//		            //setCardPos(id, x, y);
//				}
//			});
			
			$(".wrap").css({
				"margin" : getElSize(50)
			});
			
			$(".wrap").mousedown(function(){
				timeout_id = setTimeout(function() {select_remove_card()}, hold_time);
			}).bind("mouseup mouseleave", function(){
				clearTimeout(timeout_id);
			});
		}
	});
};

function closeFolder(el){
	$("#board").remove();
	$("#close_btn").remove();
	
	var folder = $(selected_folder)[0].childNodes[0];
	
	$(selected_folder).animate({
		"left" : folder_left,
		"top" : folder_top,
		"width" : folder_width,
		"height" : folder_height,
		"z-index" : 1
	});
	
	$(folder).animate({
		"width" : circleWidth - getElSize(20),
		"height" : circleWidth - getElSize(20)
	});
	
	$(".thumb").animate({
		"opacity" : 1
	});
};

function openFolder(){
	selected_folder = $(this).parent();
	
	folder_top = $(selected_folder).offset().top - getElSize(20);
	folder_left = $(selected_folder).offset().left - getElSize(20);
	folder_width = $(selected_folder).width();
	folder_height = $(selected_folder).height();
	
	var folder = $(this);
	
	$(selected_folder).css({
		"z-index" : 2
	});

	$(".thumb").animate({
		"opacity" : 0
	});

	$(selected_folder).animate({
		"left" : contentWidth * 0.05/2 + (marginWidth/2),
		"top" : contentHeight * 0.05/2 + marginHeight,
		"width" : contentWidth * 0.95,
		"height" : contentHeight * 0.95,
	}, function(){
		var board = document.createElement("div");
		board.setAttribute("id", "board");
		
		$("body").prepend(board);
		$(board).css({
			"z-index" : 3,
			"position" : "absolute",
			"left" : contentWidth * 0.05/2 + (marginWidth/2) + getElSize(30),
			"border-radius" :getElSize(30),
			"top" : contentHeight * 0.05/2 + marginHeight + getElSize(30),
			"width" : contentWidth * 0.95,
			"height" : contentHeight * 0.95,
			"overflow" : "auto"
		});
		
		var img = document.createElement("img");
		img.setAttribute("id", "close_btn");
		img.setAttribute("src", ctxPath + "/images/close.png");
		$("body").prepend(img);

		$(img).css("width", getElSize(150));
		$(img).css({
			"position" : "absolute",
			"z-index" : 4,
			"background-color" : "white",
			"border-radius" : "50%",
			"left" : ($(board).offset().left + ($(board).width())) - ($(img).width()/2),
			"top" : $(board).offset().top  - ($(img).height()/2)
		});
		
		$(img).click(closeFolder);
		
		var folderId = $(selected_folder)[0].childNodes[0].id.substr(6);

		getCardList(folderId);
	});
	
	$(folder).animate({
		"left" : 0,
		"top" : 0,
		"width" : "100%",
		"height" : "100%",
	});
};

function addDvc2Folder(obj){
	var img = $("#" + selected_circle + " img").attr("src");
	var icon = "<img src=" + img + " class='thumb'>";

	//폴더 아이콘 9개 이상이면  X
	if(chkIconCnt(obj)){
		$(obj).append(icon);
		
		$(".thumb").css({
			"width" : "28%",
			"height" :"28%",
			"margin" : getElSize(5),
			"display" : "inline",
			"-webkit-border-radius": "50%"		
		});
	};
	
	var id = obj.id.substr(6);
	var url = ctxPath + "/chart/addFolderid.do";
	var param = "id=" + selected_circle.substr(6) + 
				"&folderId=" + id;
	
	$.ajax({
		url : url,
		data : param,
		dataType : "text",
		type : "post",
		success : function(data){}
	});
};

var selected_circle;
var dropped_circle;
var border_flag = true;
var selected_folder;
var border = getElSize(20);
function borderAnim(){
	$(alarmList).each(function(idx, data){
		$("#" + data +" .circle").css({
			"box-shadow" : "0px 0px " + border + "px " + border + "px red"
		});		
		
		if (border_flag) {
			border -= (getElSize(20)/5);
			if (border <= 0) {
				border_flag = false;
			}
		} else if (!border_flag) {
			border += (getElSize(20)/5);
			if (border >= getElSize(20)) {
				border_flag = true;
			};
		};
	});
	
	setTimeout(borderAnim,100);
};

var cam_width, cam_height, cam_top, cam_left;

var cam_flag = false;
function viewCam(){
	var $cam = $("#" + this.id);

	if(!cam_flag){
		var src = $cam.attr("src");
		cam_width = $cam.width();
		cam_height = $cam.height();
		cam_top = $cam.offset().top;
		cam_left = $cam.offset().left;
		
		$cam.appendTo("body");
		
		$cam.css({
			"position": "absolute",
			"top" : cam_top,
			"left" : cam_left,
			"width" : cam_width,
			"height" : cam_height,
			"z-index" : 99999
		});
			
		$cam.animate({
			"top" : 0,
			"left" : 0,
			"width" : originWidth,
			"height" : originHeight
		});
		
		//회사 로고 & 모델명
		var idx = this.id.substr(3);
		var company;
		var model_name;
		for(var i = 0; i < machineList.length; i++){
			if(machineList[i][0]=="wrap" + idx){
				company = machineList[i][1];
				model_name = machineList[i][2];
			};
		};
		
		var logo = "<img src=" + ctxPath + "/images/company/" + company.toLowerCase() + ".png class='logo_zoom' style='height:5%' >";
		var model = "<divg  style='color:white;' class='model_name'>" + model_name + "</div>"; 
		$("body").prepend(logo);
		$("body").prepend(model);
		
		$(".logo_zoom").css({
			"position" : "absolute",
			"z-index" : 99999999,
			"left" : cam_left,
			"top" : cam_top
		});
		
		$(".model_name").css({
			"position" : "absolute",
			"z-index" : 99999999,
			"left" : cam_left + cam_width - $(".model_name").width(),
			"top" : cam_top,
			"font-size" : getElSize(70)
		});
		
		$(".logo_zoom").animate({
			"top" : 0,
			"left" : 0
		});
		
		$(".model_name").animate({
			"top" : 0,
			"left" : originWidth - ($(".model_name").width() + getElSize(50))
		});
		
	}else{
		closeCam($cam);
	};
	cam_flag = !cam_flag;
};

function closeCam(obj){
	$(".logo_zoom").remove();
	$(".model_name").remove();

	$(obj).animate({
		"width" : cam_width,
		"height" : cam_height,
		"top" : cam_top,
		"left" : cam_left
	}, function(){
		var id = obj.selector.substr(4);
		$("#cam" + id).appendTo("#wrap" + id + " .front .card_table tr:nth(1) td:nth(1)");
		$(obj).css("position","static");
	});
};

function setCardPos(id, x, y){
	var url = ctxPath + "/chart/setCardPos.do";
	var param = "id=" + id +  
				"&x=" + x +
				"&y=" + y;
	$.ajax({
		url : url,
		data : param,
		type : "post",
		success :function(data){
			
		}
	});
};
function statusBarTotal(id){
	$('#' + id).highcharts({
        chart: {
            type: 'bar',
            marginLeft:0,
            marginRight:0,
            marginTop:0,
            marginBottom:0
        },
        credits : false,
        exporting : false,
        title: {
            text: ''
        },
        xAxis: {
        	labels:{
            	enabled : false
            },
            tickLength: 0
        },
        yAxis: {
            min: 0,
            max:10,
            gridLineWidth: 0,
            reversedStacks: false,
            title: {
                text: ''
            },
            labels:{
            	enabled : true
            }
        },
        legend: {
        	enabled:false
        },
        plotOptions : {
        	bar: {
                stacking: 'normal',
                dataLabels: {
                	style : {
                		fontSize : getElSize(20),
                	},
                	enabled: true,	
                    color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'white',
                    formatter: function () {
                    	var val = this.y
                    	if(val==0){
                    		val = "";
                    	};
                    	return val;   
                    }
                }
        	},	
			series : {
				stacking : 'normal',
				pointWidth : getElSize(50),
				borderWidth : 0,
				animation : false,
				cursor : 'pointer',
			}
		},
		tooltip : {
			enabled : false
		},
        series: [{
        	color : "green",
            data: [9.6]
        },{
        	color : colors[1],
        	data: [0.2]
        },{
        	color : colors[2],
        	data: [0.1]
        },{
        	color : "gray",
        	data: [0.1]
        }]
    });
};

function statusBar(id){
	$('#' + id).highcharts({
        chart: {
            type: 'bar',
            marginLeft:0,
            marginRight:0,
            marginTop:0,
            marginBottom:0
        },
        credits : false,
        exporting : false,
        title: {
            text: ''
        },
        xAxis: {
        	labels:{
            	enabled : false
            },
            tickLength: 0
        },
        yAxis: {
            min: 0,
            max:24,
            gridLineWidth: 0,
            reversedStacks: false,
            title: {
                text: ''
            },
            labels:{
            	enabled : true
            }
        },
        legend: {
        	enabled:false
        },
        plotOptions : {
			series : {
				stacking : 'normal',
				pointWidth : getElSize(50),
				borderWidth : 0,
				animation : false,
				cursor : 'pointer',
			}
		},
		tooltip : {
			enabled : false
		},
        series: [{
        	color : "green",
            data: [5]
        },{
        	color : colors[1],
            data: [1/6*2]
        },{
        	color : colors[2],
            data: [1/6]
        },{
        	color : "green",
            data: [8]
        },{
        	color : "gray",
            data: [1/6]
        },{
        	color :"green",
            data: [1]
        }]
    });
};

var labelsArray = [ 20, 21, 22, 23, 24, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12,
		13, 14, 15, 16, 17, 18, 19, 20 ];

function statusChart3(id) {
	var perShapeGradient = {
		x1 : 0,
		y1 : 0,
		x2 : 1,
		y2 : 0
	};
	colors = Highcharts.getOptions().colors;
	colors = [ {
		linearGradient : perShapeGradient,
		stops : [ [ 0, 'rgb(100,238,92 )' ], [ 1, 'rgb(100,238,92 )' ] ]
	}, {
		linearGradient : perShapeGradient,
		stops : [ [ 0, 'rgb(250,210,80 )' ], [ 1, 'rgb(250,210,80 )' ] ]
	}, {
		linearGradient : perShapeGradient,
		stops : [ [ 0, 'rgb(231,71,79 )' ], [ 1, 'rgb(231,71,79 )' ] ]
	}, {
		linearGradient : perShapeGradient,
		stops : [ [ 0, '#8c9089' ], [ 1, '#ffffff' ] ]
	}, ]

	colors_a = Highcharts.getOptions().colors;
	colors_a = [
			{
				linearGradient : perShapeGradient,
				stops : [ [ 0, 'rgba(100,238,92, 0.5 )' ],
						[ 1, 'rgba(100,238,92,0.5 )' ] ]
			},
			{
				linearGradient : perShapeGradient,
				stops : [ [ 0, 'rgba(250,210,80, 0.5 )' ],
						[ 1, 'rgba(250,210,80, 0.5 )' ] ]
			},
			{
				linearGradient : perShapeGradient,
				stops : [ [ 0, 'rgba(231,71,79, 0.5 )' ],
						[ 1, 'rgba(231,71,79, 0.5 )' ] ]
			}, {
				linearGradient : perShapeGradient,
				stops : [ [ 0, '#8c9089' ], [ 1, '#ffffff' ] ]
			}, ]

	var height = window.innerHeight;
	$('#' + id).highcharts(
			{
				chart : {
					type : 'bar',
					backgroundColor : 'rgb(50, 50, 50)',
					height : height * 0.85
				},
				credits : false,
				exporting : false,
				title : false,
				xAxis : {
					categories : [ "U101", "P42", "JO11", "T94", "S1", "R72",
							"C34", "JA88", "AJ50", "NT40", "HPT55" ],
					labels : {
						/*
						 * formatter: function () { var value =
						 * label1[this.value] return value; },
						 */
						style : {
							fontSize : getElSize(40),
							fontWeight : "bold",
							color : "white"
						}
					}
				},
				tooltip : {
					enabled : false
				},
				yAxis : {
					min : 0,
					max : 24,
					gridLineColor : "rgb(86,215,250)",
					tickInterval : 1,
					reversedStacks : false,
					title : {
						text : false
					},
					labels : {
						formatter : function() {
							var value = labelsArray[this.value]
							return value;
						},
						style : {
							color : "white",
							fontSize : "10px"
						}
					}
				},
				legend : {
					enabled : false
				},
				plotOptions : {
					series : {
						stacking : 'normal',
						pointWidth : getElSize(150),
						borderWidth : 0,
						animation : false,
						cursor : 'pointer',
						point : {
							events : {
								click : function(e) {
									// showDetailEvent(this.startTime,this.endTime,e.clientX,
									// e.clientY)
								}
							}
						}
					}
				},
				series : [
						{
							data : [ 10, 9, 4, 5, 10, 13, 5, 6, 7, 8, 8 ],
							color : colors[0]
						},
						{
							data : [ 5, 2, 4, 5, 0, 1, 5, 6, 3, 1, 4 ],
							color : colors[1]
						},
						{
							data : [ 0, 4, 7, 5, 5, 1, 5, 3, 5, 6, 3 ],
							color : colors[2]
						},
						]
			});
	
		status3 = $("#" + id).highcharts();
};

function statusChart4(id) {
	var perShapeGradient = {
		x1 : 0,
		y1 : 0,
		x2 : 1,
		y2 : 0
	};
	colors = Highcharts.getOptions().colors;
	colors = [ {
		linearGradient : perShapeGradient,
		stops : [ [ 0, 'rgb(100,238,92 )' ], [ 1, 'rgb(100,238,92 )' ] ]
	}, {
		linearGradient : perShapeGradient,
		stops : [ [ 0, 'rgb(250,210,80 )' ], [ 1, 'rgb(250,210,80 )' ] ]
	}, {
		linearGradient : perShapeGradient,
		stops : [ [ 0, 'rgb(231,71,79 )' ], [ 1, 'rgb(231,71,79 )' ] ]
	}, {
		linearGradient : perShapeGradient,
		stops : [ [ 0, '#8c9089' ], [ 1, '#ffffff' ] ]
	}, ]

	var height = window.innerHeight;
	$('#' + id).highcharts(
			{
				chart : {
					type : 'bar',
					backgroundColor : 'rgb(50, 50, 50)',
					height : height * 0.85
				},
				credits : false,
				exporting : false,
				title : false,
				xAxis : {
					categories : [ "SA501", "TI221", "GNB41", "FW41", "SE1",
							"VMG5", "AA5G", "WWO2", "AX2", "CD3", "B0" ],
					labels : {
						/*
						 * formatter: function () { var value =
						 * label2[this.value] return value; },
						 */
						style : {
							color : "white",
							fontSize : getElSize(40),
						}
					}
				},
				tooltip : {
					enabled : false
				},
				yAxis : {
					gridLineColor : "rgb(86,215,250)",
					min : 0,
					max : 24,
					tickInterval : 1,
					reversedStacks : false,
					title : {
						text : false
					},
					labels : {
						formatter : function() {
							var value = labelsArray[this.value]
							return value;
						},
						style : {
							color : "white",
							fontSize : "10px"
						}
					}
				},
				legend : {
					enabled : false
				},
				plotOptions : {
					series : {
						stacking : 'normal',
						pointWidth : getElSize(150),
						borderWidth : 0,
						animation : false,
						cursor : 'pointer',
						point : {
							events : {
								click : function(e) {
									// showDetailEvent(this.startTime,this.endTime,e.clientX,
									// e.clientY)
								}
							}
						}
					}
				},
				series : [ {
					data : [ 8, 8, 12, 5, 10, 13, 5, 6, 9, 8, 6 ],
					color : colors[0]
				}, {
					data : [ 5, 3, 3, 5, 3, 1, 5, 6, 5, 2, 2 ],
					color : colors[1]
				}, {
					data : [ 2, 4, 0, 5, 2, 1, 5, 3, 1, 5, 7 ],
					color : colors[2]
				} ]
			});
	
	status4 = $("#" + id).highcharts();
};

function initPanel(obj) {
	var menu = obj.id;
	cPage = menu.substr(menu.lastIndexOf("u") + 1);
	$("#panel_table td:nth(" + (cPage - 1) + ")").addClass("activeMenu");
};

function flipCard_r() {
	$("#part2").append($("#mainTable2"));
	$("#back").animate({
		"width" : card_width,
		"height" : card_height,
		"top" : card_top,
		"left" : card_left,
		"background-color" : "white"
	}, function() {
		//$("#part2").append($("#mainTable2"));
		$("#back").remove();
		$(".wrap").css({
			"transition" : "0.5s",
			"-webkit-transform" : "rotateY(0deg)",
		});
	});
};

var card_width;
var card_height;
var card_top;
var card_left;

function flipCard() {
	var $wrap = $(this).parent().parent().parent().parent().parent().parent(); 
	var idx = this.id.substr(3);
	$wrap.css({
		"-webkit-transition" : "0.5s",
		"-webkit-transform" : "rotateY(180deg)",
	});

	var div = document.createElement("div");
	div.setAttribute("id", "back");
	var top = $wrap.offset().top;
	var left = $wrap.offset().left;
	var width = $wrap.width();
	var height = $wrap.height();

	var width_padding = Number($wrap.css("padding-left").substr(0,
			$wrap.css("padding-left").lastIndexOf("px"))) * 2;
	var height_padding = Number($wrap.css("padding-top").substr(0,
			$wrap.css("padding-top").lastIndexOf("px")))
			+ Number($wrap.css("padding-bottom").substr(0,
					$wrap.css("padding-bottom").lastIndexOf("px")));

	card_width = width + width_padding;
	card_height = height + height_padding;
	card_top = $wrap.offset().top;
	card_left = $wrap.offset().left;

	div.style.cssText = "background-color:white; " + "z-index:9999999; "
			+ "position:absolute; " + "top : " + top + "; " + "left:" + left
			+ "; " + "width:" + (width + width_padding) + "; " + "height:"
			+ (height + height_padding) + ";" + "border-radius : "
			+ getElSize(30) + "px " + 
			"background-images:url(../images/shopLayOut/card_back.png";

	$("#mainTable2").css("opacity", 0);

	setTimeout(function() {
		$("body").prepend(div);
		$(div).click(flipCard_r);
		$(div).animate({
			"border-radius" : 0,
			"width" : window.innerWidth,
			"height" : window.innerHeight,
			"top" : 0,
			"left" : 0,
			"background-color" : "black",
		}, 500, function() {
			$(div).append($("#mainTable2"));
			
			for(var i = 0; i < machineList.length; i++){
				if(machineList[i][0]=="wrap" + idx)idx = i;
			};
			var machine_name = machineList[idx][2];
			var img = "<img src=" + ctxPath + "/images/company/" + machineList[idx][1].toLowerCase() + ".png style='height:20%'>";
			$("#machine_name").html(img + "<br>" + machine_name.trim());
			//setEl();
			$("#mainTable2").animate({
				"opacity" : 1
			}, 500);
			
			$("#machine_name").css({
				"margin-top" : $("#machine_name_td").height() / 2
								- $("#machine_name").height() / 2
								- $(".subTitle").height()
			});
		});
	}, 500)
};

function showLabel(id){
	$("#" + id).css("opacity", 0.2);
	var width = $("#" + id).width();
	var height = $("#" + id).height();
	var left = $("#" + id).offset().left;
	var top = $("#" + id).offset().top;
	
	var div = document.createElement("div");
	div.setAttribute("id", id);
	div.setAttribute("class", "menu_label");
	
	var text = document.createTextNode(id);
	div.appendChild(text);
	$("body").prepend(div);
	
	div.style.cssText = "position : absolute;" + 
						"width : " + width + + "; " + 
						"height : " + height + "; " + 
						"z-index: 99999999;" + 
						"color:white;" + 
						"font-size : " + getElSize(50) + "; " +  
						"font-weight:bolder;"
	
	var left = left + (width/2) - ($(div).width()/2);
	var top= top + (height/2) - ($(div).height()/2);
	if(cMenu==id){
		left += getElSize(20);
		top += getElSize(20);
	};
	
	div.style.cssText += "left : " + left + ";" + 
						 "top : " + top + "; ";
};

function removeLabel(id){
	$("#" + id).remove();
	$("#" + id).css("opacity", 1);
};

var hold_time = 1000;
var timeout_id = 0;

function cancelNaming(){
	$("#nameBox").toggle(500, function(){
		$("#folder_name").val("");
	});
	
	hideCorver();
	modifyName = "";
	
	$("#" + dropped_circle + " .circle").css({
		"width" : circleWidth,
		"height" : circleWidth
	});
	$("#" + dropped_circle).show();
	//$("#" + selected_circle).show();
	getMachinePos(selected_circle.substr(6));
	$(selectedFolder).remove();
};

function getMachinePos(id){
	var url = ctxPath + "/chart/getMachinePos.do";
	var param = "id=" + id;
	
	$.ajax({
		url : url,
		data : param,
		dataType : "json",
		type : "post",
		success : function(data){
			$("#" + selected_circle).css({
				"left" : getElSize(data.x) + marginWidth,
				"top" : getElSize(data.y) + marginHeight
			}).show();
		}
	});
	
};

var upPage = new Array();
var downPage = new Array();
var cPage = 1;

function evtBind() {
	cPage = $("#panel_table td img:nth(0)").attr("id");
	$("#name_close").click(cancelNaming);
//	$("#panel_table tr img").hover(function(){
//		showLabel(this.id);
//	}, function(){
//		removeLabel(this.id);
//	});
	
	$("#folder_name").bind("keyup", function(e){
		if(e.keyCode==13){
			addFolder2DB();
		};
	});
	
	$("#menu_btn").click(togglePanel);

	$("#panel_table td img").click(function() {
//		$(".subMenu").remove();
//		$(".menu_label").hide();
//		if(this.id!="menu4"){
//			togglePanel();
//			subMenuTr = false;
//		};
		$("#panel_table img").css("border",0)
		$(this).css({
			"border" : getElSize(20) + "px solid rgb(33,128,250)"
		});
		
		var id = this.id;
		
		if(cPage==id){
			$("#menu_btn").click();
			return;
		};
		
		$(".video").each(function(idx, data){
			$(this).get(0).pause();
		});
		
		if(id=="DashBoard_1"){
//			$(".video").each(function(idx, data){
//				$(this).get(0).play();
//			});
		};
		
		console.log(cPage)
		
		upPage.push(cPage);
		var page = cPage.substr(cPage.lastIndexOf("_")+1);
		
		$("#part" + page).animate({
			"top" : - originHeight
		});
		
		cPage = id;
		
		page = id.substr(cPage.lastIndexOf("_")+1);

		$("#part" + page).animate({
			"top" : 0
		});
		
		togglePanel();
	});
		//initPanel(this);

//		$("#panel_table td").removeClass("activeMenu");
//		$("#panel_table td").removeClass("selected_menu");
//		$("#panel_table td").addClass("unSelected_menu");
//		$("#panel_table td:nth(" + (cPage - 1) + ")").addClass("activeMenu")

		// page slide
//		if (cPage == 1) {
//			$("#part1").animate({
//				"top" : 0
//			});
			// $("#part2").animate({
			// "top" : originHeight
			// });
			// $("#part3").animate({
			// "top" : originHeight*2
			// });
			// $("#part4").animate({
			// "top" : originHeight*3
			// });

//			for (var i = 2; i <= 5; i++) {
//				$("#part" + i).animate({
//					"top" : originHeight * (i - 1)
//				})
//			};
//		} else if (cPage == 2) {
//			$("#part1").animate({
//				"top" : -originHeight
//			});
//			$("#part2").animate({
//				"top" : 0
//			});
//			$("#part3").animate({
//				"top" : originHeight
//			});
//			$("#part4").animate({
//				"top" : originHeight * 2
//			});
//			$("#part5").animate({
//				"top" : originHeight * 3
//			});
//		} else if (cPage == 3) {
//			$("#part1").animate({
//				"top" : -(originHeight * 2)
//			});
//			$("#part2").animate({
//				"top" : -originHeight
//			});
//			$("#part3").animate({
//				"top" : 0
//			});
//			$("#part4").animate({
//				"top" : originHeight
//			});
//			$("#part5").animate({
//				"top" : originHeight * 2
//			});
//		} else if (cPage == 4) {
//			if(!subMenuTr){
//				var subMenu = "<tr class='subMenu'><td>⌊ SUBMENU1</td></tr>" +
//							"<tr class='subMenu'><td>⌊ SUBMENU2</td></tr>";
//				
//				$('#panel_table > tbody > tr').eq(3).after(subMenu);
//				
//				$(".subMenu").css({
//					"cursor" : "pointer",
//					"font-size" : getElSize(50),
//					"display" : "none",
//					"background-color" : "#CECCCC"
//				});
//				
//				$(".subMenu").hover(function(){
//					$(this).addClass("selected_menu")
//				}, function(){
//					$(this).removeClass("selected_menu")
//				});
//				$(".subMenu").slideDown("slow");
//			};
//			
//			subMenuTr = !subMenuTr;
			// $("#part1").animate({
			// "top" : -(originHeight*3)
			// });
			// $("#part2").animate({
			// "top" : -(originHeight*2)
			// });
			// $("#part3").animate({
			// "top" : -originHeight
			// });
			// $("#part4").animate({
			// "top" : 0
			// });
			// $("#part5").animate({
			// "top" : originHeight
			// });
//		} else if (cPage == 5) {
//			$("#part1").animate({
//				"top" : -(originHeight * 4)
//			});
//			$("#part2").animate({
//				"top" : -(originHeight * 3)
//			});
//			$("#part3").animate({
//				"top" : -(originHeight * 2)
//			});
//			$("#part4").animate({
//				"top" : -(originHeight)
//			});
//			$("#part5").animate({
//				"top" : 0
//			});
//		};
//	});
//
//	$("#corver").click(function() {
//		//if (panel)togglePanel();
//	});

//	$("#panel_table td").hover(
//			function() {
//				$("#" + this.id).removeClass("unSelected_menu");
//				$("#" + this.id).addClass("selected_menu");
//			},
//			function() {
//				if ($(this).html() == $(
//						"#panel_table td:nth(" + (cPage - 1) + ")").html()) {
//					$(this).addClass("activeMenu");
//				} else {
//					$("#" + this.id).removeClass("selected_menu");
//					$("#" + this.id).addClass("unSelected_menu");
//				}
//				;
//			});
	
	
};

function togglePanel() {
	var panelDist;
	var btnDist;

	if (panel) {
		panelDist = -(originWidth * 0.2) - getElSize(20) * 2;
		btnDist = getElSize(30);
		
		hideCorver();
	} else {
		panelDist = 0;
		btnDist = (originWidth * 0.2) + ($("#menu_btn").width() / 3.5)
				+ getElSize(20);
		
		showCorver();
	};

	panel = !panel;

	$("#panel").animate({
		"left" : panelDist
	});
	$("#menu_btn").animate({
		"left" : btnDist
	});
};

function drawBarChart(id) {
	var perShapeGradient = {
		x1 : 0,
		y1 : 0,
		x2 : 1,
		y2 : 0
	};
	colors = Highcharts.getOptions().colors;
	colors = [ {
		linearGradient : perShapeGradient,
		stops : [ [ 0, 'rgb(100,238,92 )' ], [ 1, 'rgb(100,238,92 )' ] ]
	}, {
		linearGradient : perShapeGradient,
		stops : [ [ 0, 'rgb(250,210,80 )' ], [ 1, 'rgb(250,210,80 )' ] ]
	}, {
		linearGradient : perShapeGradient,
		stops : [ [ 0, 'rgb(231,71,79 )' ], [ 1, 'rgb(231,71,79 )' ] ]
	}, {
		linearGradient : perShapeGradient,
		stops : [ [ 0, '#8C9089' ], [ 1, '#8C9089' ] ]
	}, ]

	var height = window.innerHeight;

	var options = {
		chart : {
			type : 'coloredarea',
			backgroundColor : 'rgb(50,50,50)',
			height : height * 0.38,
			marginTop : -(height * 0.2),
		},
		credits : false,
		title : false,
		xAxis : {
			categories : [ 20, 0, 0, 0, 0, 0, 21, 0, 0, 0, 0, 0, 22, 0, 0, 0,
					0, 0, 23, 0, 0, 0, 0, 0, 24, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0,
					0, 2, 0, 0, 0, 0, 0, 3, 0, 0, 0, 0, 0, 4, 0, 0, 0, 0, 0, 5,
					0, 0, 0, 0, 0, 6, 0, 0, 0, 0, 0, 7, 0, 0, 0, 0, 0, 8, 0, 0,
					0, 0, 0, 9, 0, 0, 0, 0, 0, 10, 0, 0, 0, 0, 0, 11, 0, 0, 0,
					0, 0, 12, 0, 0, 0, 0, 0, 13, 0, 0, 0, 0, 0, 14, 0, 0, 0, 0,
					0, 15, 0, 0, 0, 0, 0, 16, 0, 0, 0, 0, 0, 17, 0, 0, 0, 0, 0,
					18, 0, 0, 0, 0, 0, 19, 0, 0, 0, 0, 0, 20, 0, 0, 0, 0, 0, ],
			labels : {

				formatter : function() {
					var val = this.value
					if (val == 0) {
						val = "";
					}
					;
					return val;
				},
				style : {
					color : "white",
					fontSize : getElSize(15),
					fontWeight : "bold"
				},
			}
		},
		yAxis : {
			labels : {
				enabled : false,
			},
			title : {
				text : false
			},
		},
		tooltip : {
			enabled : false
		},
		plotOptions : {
			line : {
				marker : {
					enabled : false
				}
			}
		},
		legend : {
			enabled : false
		},
		series : []
	};

	$("#" + id).highcharts(options);

	var status = $("#container").highcharts();
	var options = status.options;

	options.series = [];
	options.title = null;
	options.exporting = false;

	options.series.push({
		data : [ {
			y : Number(200),
			segmentColor : colors[0]
		} ],
	});

	for (var i = 0; i < 9; i++) {
		options.series[0].data.push({
			y : Number(200),
			segmentColor : colors[3]
		});
	}

	for (var i = 0; i < 40; i++) {
		options.series[0].data.push({
			y : Number(200),
			segmentColor : colors[0]
		});
	}

	for (var i = 0; i < 50; i++) {
		options.series[0].data.push({
			y : Number(200),
			segmentColor : colors[1]
		});
	}

	for (var i = 0; i < 44; i++) {
		options.series[0].data.push({
			y : Number(200),
			segmentColor : colors[2]
		});
	}

	status = new Highcharts.Chart(options);
};

function timeConverter(UNIX_timestamp) {
	var a = new Date(UNIX_timestamp * 1000);
	var months = [ 'Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug',
			'Sep', 'Oct', 'Nov', 'Dec' ];
	var year = a.getFullYear();
	var month = months[a.getMonth()];
	var date = a.getDate();
	var hour = a.getHours();
	var min = a.getMinutes();
	var sec = a.getSeconds();
	var time = date + ' ' + month + ' ' + year + ' ' + hour + ':' + min + ':'
			+ sec;
	return time;
}

function pieChart() {
	Highcharts.setOptions({
		// green yellow red gray
		colors : [ 'rgb(100,238,92 )', 'rgb(250,210,80 )', 'rgb(231,71,79 )',
				'#8C9089' ]
	});

	$('#pie')
			.highcharts(
					{
						chart : {
							plotBackgroundColor : null,
							plotBorderWidth : null,
							plotShadow : false,
							type : 'pie',
							backgroundColor : "rgba(0,0,0,0)"
						},
						credits : false,
						exporting : false,
						title : {
							text : false
						},
						legend : {
							enabled : false
						},
						tooltip : {
							pointFormat : '{series.name}: <b>{point.percentage:.1f}%</b>'
						},
						plotOptions : {
							pie : {
								allowPointSelect : true,
								cursor : 'pointer',
								dataLabels : {
									enabled : true,
									format : '<b>{point.name}</b>: {point.percentage:.1f} %',
									style : {
										color : (Highcharts.theme && Highcharts.theme.contrastTextColor)
												|| 'white',
										fontSize : getElSize(30)
									}
								},
								showInLegend : true
							}
						},
						series : [ {
							name : "Brands",
							colorByPoint : true,
							data : [ {
								name : "In-Cycle",
								y : 56.33
							}, {
								name : "Wait",
								y : 24.03
							}, {
								name : "Alarm",
								y : 10.38
							}, {
								name : "No-Connection",
								y : 4.77
							} ]
						} ]
					});
};

function setEl() {
	$(".container").css({
		"width": contentWidth,
		"height" : contentHeight,
		"margin-top" : originHeight/2 - ($(".container").height()/2)
	});
	
	$(".container").css({
		"margin-top" : originHeight/2 - ($(".container").height()/2),
		"margin-left" : originWidth/2 - ($(".container").width()/2),
	});
	
	$(".layoutTd").css({
		height : contentHeight * 0.85
	});

	$(".page").css({
		"height" : originHeight
	});

	$("#part1").css({
		"top" : 0
	});

	// 페이지 위치 조정
	for (var i = 2; i <= 5; i++) {
		$("#part" + i).css({
			"top" : $("#part" + (i - 1)).height() + originHeight
		});
	};

	$(".mainTable").css({
		//"border-spacing" : getElSize(40)
	});

	$(".title").css({
		"padding" : getElSize(40),
		"font-size" : getElSize(70)
	});

	$(".subTitle").css({
		"padding" : getElSize(40),
		"font-size" : getElSize(50)
	});

	$(".tr2").css({
		"height" : contentHeight * 0.35
	})

	$("#chartTr").css({
		"height" : contentHeight * 0.4
	});

	$("#pie").css({
		"height" : contentHeight * 0.32
	});

	$(".neon1, .neon2, .neon3").css({
		"font-size" : getElSize(500)
	});

	$(".neon1, .neon2, .neon3").css({
		"margin-top" : $(".tr2").height() / 2 - $(".neon1").height()
						/ 2 - $(".subTitle").height(),
	});

	$("#machine_name").css({
		"color" : "white",
		"font-weight" : "bolder",
		"font-size" : getElSize(170),
	});

	$("#machine_name_td").css({
		"height" : getElSize(500)
	});

	$("#machine_name").css({
		"margin-top" : $("#machine_name_td").height() / 2
						- $("#machine_name").height() / 2
						- $(".subTitle").height()
	});

	$(".title_left").css({
		"float" : "left",
		"width" : contentWidth * 0.1,
		"margin-left" : getElSize(150)
	});

	$(".title_right").css({
		"float" : "right",
		"width" : contentWidth * 0.1
	});

	$("#menu_btn").css({
		"width" : contentWidth * 0.06,
		"top" : marginHeight + (contentHeight * 0.01),
		"left" : getElSize(30),
		"z-index" : 5
	});

	$("#panel").css({
		"height" : originHeight,
		"background-color" : "rgb(34,34,34)",
		"border" : getElSize(20) + "px solid rgb(50,50,50)",
		"width" : contentWidth * 0.2,
		"position" : "absolute",
		"color" : "white",
		"z-index" : 999999,
		"left" : -contentWidth * 0.2 - (getElSize(20) * 2)
	});

	$("#corver").css({
		"width" : originWidth,
		"height" : originHeight,
		"position" : "absolute",
		"z-index" : -1,
		"background-color" : "black",
		"opacity" : 0,
	});

	$("#panel_table").css({
		"color" : "white",
		"font-size" : getElSize(60)
	});

	$("#panel_table td").css({
		"padding" : getElSize(50),
		"cursor" : "pointer"
	});

	//$("#panel_table td").addClass("unSelected_menu");

	$(".dvcName_p1").css({
		"font-weight" : "bolder",
		"font-size" : getElSize(40)
	});

	
	$(".wrap").css({
		"width" : contentWidth * 0.2,
		"padding" : getElSize(10),
		"border-radius" : getElSize(30),
		"padding-bottom" : getElSize(10),
	});
	
	$(".waitCnt, .alarmCnt, .noConCnt").css({
		"font-size" : getElSize(20),
		"margin" :getElSize(20)
	});
	
	$(".waitCnt").css({
		"color" : "yellow"
	});
	
	$(".alarmCnt").css({
		"color" : "red"
	});
	
	$(".noConCnt").css({
		"color" : "gray"
	});
	
	var chart_height = getElSize(60);
	if(chart_height<20) chart_height = 20;
	
	$(".card_status").css({
		"height" : chart_height,
	});
	
	$(".card_table").css({
		"border-spacing" : getElSize(10)
	});
	
	$(".opRatio").css({
		"font-size" :getElSize(100)
	});
	
	$(".comName").css({
		"font-size" :getElSize(60)
	});
	
	$(".machine_cam").css({
		"width" : contentWidth * 0.07,
		"height" : contentHeight * 0.08,
		//"border" : getElSize(10) + "px solid rgb(50,50,50)" 
	});
	
	$(".frame").css({
		"width" : contentWidth * 0.07,
		"height" : contentHeight * 0.15
	});
	
	$(".statusCnt").css({
		"height" : getElSize(70),
		"vertical-align" : "bottom",
		"box-shadow ": "inset 0 5px 0 #ddd"
	});
	
	$(".opTime").css({
		"font-weight" : "bolder",
		"top" : getElSize(400),
		"right" : getElSize(10)
	});
	
	$(".logo").css({
		"height" : contentWidth * 0.015
	});
	
	$(".verticalLine").css({
		"width" : getElSize(5),
		"background-color" : "rgb(86,215,250)",
		"box-shadow" : "0px 0px " + getElSize(5) + "px " + getElSize(5) + "px rgb(86,215,250)",
		"position": "absolute",
		"display" : "none",
		"top" : $("#status3_1").offset().top - (originHeight*2) + 10,
	});
	
	$(".machine_icon").css({
		"position" : "absolute",
		"margin" : getElSize(20),
		"width" : contentWidth * 0.1,
		"height" : contentWidth * 0.1,
		"cursor" : "pointer"
	});
	
	$(".circle").css({
		"background-color" : "white",
		"width" : contentWidth * 0.07,
		"height" : contentWidth * 0.07,
		"border-radius" : "50%",
	});
	
	$("#nameBox").css({
		"position" : "absolute",
		"width" : contentWidth * 0.2,
	});
	$("#nameBox").css({
		"left" : contentWidth/2 - ($("#nameBox").width()/2) + marginWidth,
		"top" : contentHeight/2 - ($("#nameBox").height()/2) + marginHeight,
		"z-index" : 4,
		"display" : "none"
	});
	
	$("#folder_name").css({
		"height" : getElSize(100),
		"width" : "80%",
		"font-size" : getElSize(50),
		"border-radius" : getElSize(20),
		"outline" : 0,
		"border" : getElSize(10) + "px solid rgb(235,234,219)",
	});
	
	$("#folder_name").focus(function(){
		$(this).css({
			"border" : getElSize(10) + "px solid rgb(35,137,247)"
		});
	});
	
	$("#folder_name").blur(function(){
		$(this).css({
			"border" : getElSize(10) + "px solid rgb(235,234,219)"
		});
	});
	
	$(".menu_icon").css({
		"width" : getElSize(250),
		"background-color" : "white",
		"border-radius" : "50%"
	});
	
	$("#name_close").css({
		"position" : "absolute",
		"width" : getElSize(70),
		"background-color" : "white",
		"border-radius" : "50%",
		"right" : 0,
		"cursor" : "pointer"
	});
};

function drawStockChart() {
	var seriesOptions = [], seriesCounter = 0, names = [ 'MSFT', 'AAPL', 'GOOG' ],
	// create the chart when all data is loadedx
	createChart = function() {
		$('#container').highcharts('StockChart', {
			chart : {
				height : originHeight * 0.45
			},
			exporting : false,
			credits : false,
			rangeSelector : {
				selected : 4
			},

			rangeSelector : {
				buttons : [ {
					type : 'hour',
					count : 1,
					text : '1h'
				}, {
					type : 'day',
					count : 1,
					text : '1d'
				}, {
					type : 'month',
					count : 1,
					text : '1m'
				}, {
					type : 'year',
					count : 1,
					text : '1y'
				}, {
					type : 'all',
					text : 'All'
				} ],
				inputEnabled : true, // it supports only days
				selected : 4
			// all
			},

			yAxis : {
				labels : {
					formatter : function() {
						// return (this.value > 0 ? ' + ' : '') + this.value +
						// '%';
						return this.value
					}
				},
				plotLines : [ {
					value : 0,
					width : 2,
					color : 'silver'
				} ]
			},

			/*
			 * plotOptions: { series: { compare: 'percent' } },
			 */

			tooltip : {
				// pointFormat: '<span
				// style="color:{series.color}">{series.name}</span>:
				// <b>{point.y}</b> ({point.change}%)<br/>',
				valueDecimals : 2
			},

			series : seriesOptions
		});
	};

	seriesOptions[0] = {
		name : names[0],
		data : data_
	};

	/*
	 * $.each(names, function (i, name) {
	 * 
	 * $.getJSON('http://www.highcharts.com/samples/data/jsonp.php?filename=' +
	 * name.toLowerCase() + '-c.json&callback=?', function (data) {
	 * seriesOptions[i] = { name: name, data: data_ };
	 * 
	 * seriesCounter += 1;
	 * 
	 * if (seriesCounter === names.length) { createChart(); } }); });
	 */

	Highcharts.createElement('link', {
		href : '//fonts.googleapis.com/css?family=Unica+One',
		rel : 'stylesheet',
		type : 'text/css'
	}, null, document.getElementsByTagName('head')[0]);

	Highcharts.theme = {
		colors : [ "#2b908f", "#90ee7e", "#f45b5b", "#7798BF", "#aaeeee",
				"#ff0066", "#eeaaee", "#55BF3B", "#DF5353", "#7798BF",
				"#aaeeee" ],
		chart : {
			backgroundColor : {
				linearGradient : {
					x1 : 0,
					y1 : 0,
					x2 : 1,
					y2 : 1
				},
				stops : [ [ 0, '#2a2a2b' ], [ 1, '#3e3e40' ] ]
			},
			style : {
				fontFamily : "'Unica One', sans-serif"
			},
			plotBorderColor : '#606063'
		},
		title : {
			style : {
				color : '#E0E0E3',
				textTransform : 'uppercase',
				fontSize : '20px'
			}
		},
		subtitle : {
			style : {
				color : '#E0E0E3',
				textTransform : 'uppercase'
			}
		},
		xAxis : {
			gridLineColor : '#707073',
			labels : {
				style : {
					color : '#E0E0E3'
				}
			},
			lineColor : '#707073',
			minorGridLineColor : '#505053',
			tickColor : '#707073',
			title : {
				style : {
					color : '#A0A0A3'

				}
			}
		},
		yAxis : {
			gridLineColor : '#707073',
			labels : {
				style : {
					color : '#E0E0E3'
				}
			},
			lineColor : '#707073',
			minorGridLineColor : '#505053',
			tickColor : '#707073',
			tickWidth : 1,
			title : {
				style : {
					color : '#A0A0A3'
				}
			}
		},
		tooltip : {
			backgroundColor : 'rgba(0, 0, 0, 0.85)',
			style : {
				color : '#F0F0F0'
			}
		},
		plotOptions : {
			series : {
				dataLabels : {
					color : '#B0B0B3'
				},
				marker : {
					lineColor : '#333'
				}
			},
			boxplot : {
				fillColor : '#505053'
			},
			candlestick : {
				lineColor : 'white'
			},
			errorbar : {
				color : 'white'
			}
		},
		legend : {
			itemStyle : {
				color : '#E0E0E3'
			},
			itemHoverStyle : {
				color : '#FFF'
			},
			itemHiddenStyle : {
				color : '#606063'
			}
		},
		credits : {
			style : {
				color : '#666'
			}
		},
		labels : {
			style : {
				color : '#707073'
			}
		},

		drilldown : {
			activeAxisLabelStyle : {
				color : '#F0F0F3'
			},
			activeDataLabelStyle : {
				color : '#F0F0F3'
			}
		},

		navigation : {
			buttonOptions : {
				symbolStroke : '#DDDDDD',
				theme : {
					fill : '#505053'
				}
			}
		},

		// scroll charts
		rangeSelector : {
			buttonTheme : {
				fill : '#505053',
				stroke : '#000000',
				style : {
					color : '#CCC'
				},
				states : {
					hover : {
						fill : '#707073',
						stroke : '#000000',
						style : {
							color : 'white'
						}
					},
					select : {
						fill : '#000003',
						stroke : '#000000',
						style : {
							color : 'white'
						}
					}
				}
			},
			inputBoxBorderColor : '#505053',
			inputStyle : {
				backgroundColor : '#333',
				color : 'silver'
			},
			labelStyle : {
				color : 'silver'
			}
		},

		navigator : {
			handles : {
				backgroundColor : '#666',
				borderColor : '#AAA'
			},
			outlineColor : '#CCC',
			maskFill : 'rgba(255,255,255,0.1)',
			series : {
				color : '#7798BF',
				lineColor : '#A6C7ED'
			},
			xAxis : {
				gridLineColor : '#505053'
			}
		},

		scrollbar : {
			barBackgroundColor : '#808083',
			barBorderColor : '#808083',
			buttonArrowColor : '#CCC',
			buttonBackgroundColor : '#606063',
			buttonBorderColor : '#606063',
			rifleColor : '#FFF',
			trackBackgroundColor : '#404043',
			trackBorderColor : '#404043'
		},

		// special colors for some of the
		legendBackgroundColor : 'rgba(0, 0, 0, 0.5)',
		background2 : '#505053',
		dataLabelsColor : '#B0B0B3',
		textColor : '#C0C0C0',
		contrastTextColor : '#F0F0F3',
		maskColor : 'rgba(255,255,255,0.3)'
	};

	// Apply the theme
	Highcharts.setOptions(Highcharts.theme);
	createChart();
};